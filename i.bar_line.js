/*!
 * iChart - Charting library, based on Raphaël
 *
 * Copyright (c) 2012 Iuri Brito
 * Licensed under the MIT (http://www.opensource.org/licenses/mit-license.php) license.
 */

(function () {
  function ibarline(p, width, height, ArrValuesBar, ArrValuesLine, opt) {
    var green = "90-#5B8433-#8CC53E", orange = "90-#E37201-#FC9B03", blue = "90-#006699-#00AEEF";
    var colorsConstant = ["#8CC53E", "#FC9B03", "#00AEEF"];
    var colors = clone(colorsConstant);

    var widthBar = 0;
    var paddingTop = 30;
    var linhasBack = 9;

    var values = [];
    values = clone(ArrValuesBar);

    var valuesLine = [];
    valuesLine = clone(ArrValuesLine);

    var iBarChart = p.rect(30, 60, width, height);
    var iBarInfo = iBarChart.getBBox();

    var chartBarLine = this;

    //Barra
    var maxValue = maxValueArray(values);//Math.max.apply( Math, values[0] );
    var minValue = minValueArray(values);//Math.min.apply( Math, values[0] );
    var minPainel = 0;
    var maxPainel = maxValue + paddingTop;
    var valueUnitAxisY = maxPainel / linhasBack;
    //Linha
    var maxValueLine = maxValueArray(valuesLine);//Math.max.apply( Math, values[0] );
    var minValueLine = minValueArray(valuesLine);//Math.min.apply( Math, values[0] );
    var minPainelLine = 0;
    var maxPainelLine = maxValueLine + paddingTop;
    var valueUnitAxisYLine = maxPainelLine / linhasBack;

    var maxItensTable = 13;
    var widthAxisX = (maxItensTable <= values[0].length)? width / maxItensTable : width / values[0].length;
    var numClickT = 0;
    var clickMax = values[0].length - maxItensTable;
    var posBar = 0;

    var bar = p.set();
    var lines = p.set();
    var areaBars = p.set();
    var circleLine = p.set();
    var AxisXLabels = p.set();
    var buttonsColumn = p.set();
    var labelsBars = p.set();
    var labelsLine = p.set();

    var groupButtons = [];
    var valorColuna = [];
    var strPath = [];

    heightBackBar = height / linhasBack;

    labelsBars.push(p.text(iBarInfo.x + 30, iBarInfo.y - 50, "Item 1")
      .attr({font: "16px 'Open Sans', Open-Sans, sans-serif", fill: colors[0], cursor: "pointer", "font-weight": "600"})
      .click(function(){
        if(this.selected){
          this.selected = false;
        } else {
          this.selected = true;
        }

        chartBarLine.ocultarBar();
      })
    );
    labelsBars.push(p.text(iBarInfo.x + 90, iBarInfo.y - 50, "Item 2")
      .attr({font: "16px 'Open Sans', Open-Sans, sans-serif", fill: colors[1], cursor: "pointer", "font-weight": "600"})
      .click(function(){
        if(this.selected){
          this.selected = false;
        } else {
          this.selected = true;
        }

        chartBarLine.ocultarBar();
      })
    );
    labelsBars.push(p.text(iBarInfo.x + 150, iBarInfo.y - 50, "Item 3")
      .attr({font: "16px 'Open Sans', Open-Sans, sans-serif", fill: colors[2], cursor: "pointer", "font-weight": "600"})
      .click(function(){
        if(this.selected){
          this.selected = false;
        } else {
          this.selected = true;
        }

        chartBarLine.ocultarBar();
      })
    );

    labelsLine.push(p.text(iBarInfo.x + 300 + 30, iBarInfo.y - 50, "Item 1")
      .attr({font: "16px 'Open Sans', Open-Sans, sans-serif", fill: colors[0], cursor: "pointer", "font-weight": "600"})
      .click(function(){
        if(this.selected){
          this.selected = false;
        } else {
          this.selected = true;
        }

        chartBarLine.ocultarLine();
      })
    );
    labelsLine.push(p.text(iBarInfo.x+ 300 + 90, iBarInfo.y - 50, "Item 2")
      .attr({font: "16px 'Open Sans', Open-Sans, sans-serif", fill: colors[1], cursor: "pointer", "font-weight": "600"})
      .click(function(){
        if(this.selected){
          this.selected = false;
        } else {
          this.selected = true;
        }

        chartBarLine.ocultarLine();
      })
    );
    labelsLine.push(p.text(iBarInfo.x + 300 + 150, iBarInfo.y - 50, "Item 3")
      .attr({font: "16px 'Open Sans', Open-Sans, sans-serif", fill: colors[2], cursor: "pointer", "font-weight": "600"})
      .click(function(){
        if(this.selected){
          this.selected = false;
        } else {
          this.selected = true;
        }

        chartBarLine.ocultarLine();
      })
    );

    /*
    / Desenhar Background
    */

    for(var i = 0; i <= linhasBack; i++){
      if(i % 2 == 0 && i != linhasBack){
        p.rect(iBarInfo.x + 1, iBarInfo.y + heightBackBar * i, iBarInfo.width - 2, heightBackBar).attr({fill: "#eaeaea", "stroke-width": 0});
      }

      var txtattr = { font: "11px 'Fontin Sans', Fontin-Sans, sans-serif" };
      L = p.text(iBarInfo.x - 15, (iBarInfo.y + iBarInfo.height) - heightBackBar * i , Math.round(valueUnitAxisY * i)).attr(txtattr);
    }

    for(var i = 0; i <= linhasBack; i++){
      var txtattr = { font: "11px 'Fontin Sans', Fontin-Sans, sans-serif" };
      L = p.text(iBarInfo.x + iBarInfo.width + 15, (iBarInfo.y + iBarInfo.height) - heightBackBar * i , Math.round(valueUnitAxisYLine * i)).attr(txtattr);
    }

    p.path("M"+(iBarInfo.x + 0.5)+","+(iBarInfo.y - 30 + 0.5)+"L"+(iBarInfo.x+iBarInfo.width + 0.5)+","+(iBarInfo.y - 30 + 0.5)).attr({stroke: "#aaaaaa"});

    p.path("M"+iBarInfo.x+","+iBarInfo.y+"L"+(iBarInfo.x+iBarInfo.width)+","+iBarInfo.y).attr({stroke: "#aaaaaa"});
    p.path("M"+iBarInfo.x+","+(iBarInfo.y+iBarInfo.height)+"L"+(iBarInfo.x+iBarInfo.width)+","+(iBarInfo.y+iBarInfo.height)).attr({stroke: "#aaaaaa"});

    /*
    / Açoes nas Colunas e Label com os valores
    */

    for(var i = 1; i < values[0].length; i++) {
      p.path("M"+(iBarInfo.x+widthAxisX * i)+","+iBarInfo.y+"L"+(iBarInfo.x+widthAxisX * i)+","+(iBarInfo.y+iBarInfo.height)).attr({stroke: "#aaaaaa"});
    }

    for(var i = 0; i < values[0].length; i++){
      areaBars.push(barTemp = p.rect(iBarInfo.x + widthAxisX * i, iBarInfo.y, widthAxisX, iBarInfo.height)).attr({fill: '#444', opacity: '0'});
      areaBars[i].values = [];
      for(var j = 0; j < values.length; j++){
        areaBars[i].values.push(values[j][i]);
      }
    }

    for(var i = 0; i < areaBars.length; i++){        
      areaBars[i].mouseover(function(){
        this.label = p.set();
        var element = this;

        for(var i = 0; i < element.values.length; i++){
          var boxColunas = element.getBBox();
          var multi = (15 * i) + boxColunas.y + 20;
          var color = ["#006699", "#66CC00", "#FF6600", "transparent"];
          
          txtattr = { font: "14px 'Fontin Sans', Fontin-Sans, sans-serif", fill: color[i] };
          this.label.push(p.text(boxColunas.x + (boxColunas.width / 2), multi, element.values[i]).attr(txtattr));
        }
      }).mouseout(function(){
        this.label.remove();
      });
    }

    /*
    / Botoes de Comentarios
    */

    for(var i = 0; i < areaBars.length; i++){
      this.buttonsColumn = p.set();
      var ElementBarArea = areaBars[i].getBBox();

      this.buttonSet =  p.rect(ElementBarArea.x, ElementBarArea.y - 30, widthAxisX, 30).attr({fill: "#ccc", 'fill-opacity': 0  , stroke: '#ccc','stroke-width': 1, 'stroke-opacity':0});;
      this.buttonSet.back = p.rect(ElementBarArea.x + 0.5, ElementBarArea.y - 30 + 0.5, widthAxisX, 30).attr({'stroke-width': 1, stroke: 'transparent', fill: '#fff'});
      var elementData = this.buttonSet.getBBox();
      this.buttonSet.icon = p.image("/assets/universal/comment.png", elementData.x + elementData.width / 2 - 8, elementData.y + elementData.height / 2 - 7, 16, 15).attr({opacity: .6});
      this.buttonSet.toFront();

      this.buttonsColumn.push(this.buttonSet);

      this.buttonSet.mouseover(function(){
        this.attr({cursor: 'pointer'});
        this.back.attr({fill: "90-#EAEAEA-#FFFFFF", stroke: '#ccc'});
        this.icon.attr({opacity: 1});
      })
      .mouseout(function(){
        this.back.attr({fill: "#fff", stroke: 'transparent'});
        this.icon.attr({opacity: .6});
      })
      .click(function(){
        alert('faz alguma coisa');
      });
    }

    /*
    / Criar Barras do Grafico
    */

    this.criarBarras = function(){
      bar.remove();
      bar = p.set();
      iBarChart.attr({"stroke-width": 0});
      widthBar = (widthAxisX - 40) / values.length;

      for(var i = 0; i < values.length; i++ ) {
        for(var j = 0; j < values[i].length; j++ ){
          var labelValue = p.set();
          heightPercent = 100 * values[i][j] / maxPainel;
          heightPixel = height * heightPercent / 100;
          posBar = ((widthAxisX * (j + 1) - widthAxisX) + 20) + widthBar * i;
          bar.push(p.roundedRectangle(iBarInfo.x + posBar, iBarInfo.y + height - heightPixel, widthBar, heightPixel, 5, 5, 0, 0)
            .attr({"stroke-width": '0', fill: colors[i]})
          );
        }
      }
      lines.toFront();
      circleLine.toFront();
    }

    /*
    / Criar Linhas do Grafico
    */

    this.criarLinhas = function(){
      circleLine.remove();
      lines.remove();
      circleLine = p.set();
      lines = p.set();
      for(var i = 0; i < valuesLine.length; i++ ) {
        strPath = [];
        for(var j = 0; j < valuesLine[i].length; j++ ){
          heightPercent = 100 * valuesLine[i][j] / maxPainelLine;
          heightPixel = iBarInfo.y + height - (height * heightPercent / 100);
          posBar = (((widthAxisX * (j + 1) - widthAxisX) + 20) + widthBar * i) + widthBar / 2;//iBarInfo.x + (widthAxisX * j) + (widthAxisX / 2);


          strPath.push(iBarInfo.x + posBar+","+ heightPixel);
          circleLine.push(p.circle(iBarInfo.x + posBar, heightPixel, 5).attr({fill: colors[i], stroke: "#fff"}));
        }
        var newStringPath = "";
        for(var l = 0; l < strPath.length; l++){
          if(l == 0) {
            newStringPath += "M"+strPath[l]+" R";
          } else if( l == strPath.length - 1) {
            newStringPath += strPath[l];
          } else {
            newStringPath += strPath[l]+" ";
          }
        }
        lines.push(p.path(newStringPath).attr({stroke: colors[i], "stroke-width": 2}));
      }
      circleLine.toFront();
    }

    /*
    / Criar Labels do Grafico
    */

    areaBars.toFront();
    labels = ['JAN', 'FEV', 'MAR', 'ABR', 'MAI', 'JUN', 'JUL', 'AGO', 'SET', 'OUT'];
    for ( var i = 0; i < labels.length; i++) {
      var barL = areaBars[i], 
          bb = barL.getBBox(),
          txtattr = { font: "11px 'Fontin Sans', Fontin-Sans, sans-serif"};

      if(labels[i]) {
        L = p.text((bb.x) + widthAxisX / 2, bb.y + bb.height + 20, labels[i]).attr(txtattr);
      }
    }

    this.ocultarBar = function(){
      values = [];
      colors = [];
      console.log(values);
      for(var i =0; i < labelsBars.length; i++){
        if(!labelsBars[i].selected){
          values.push(ArrValuesBar[i]);
          colors.push(colorsConstant[i]);
        }
      }
      console.log(values);


      chartBarLine.criarBarras();
    }

    this.ocultarLine = function(){
      valuesLine = [];
      colors = [];
      for(var i =0; i < labelsLine.length; i++){
        if(!labelsLine[i].selected){
          valuesLine.push(ArrValuesLine[i]);
          colors.push(colorsConstant[i]);
        }
      }

      chartBarLine.criarLinhas();
    }

    this.navNext = function(){
      if(clickMax != numClickT){
        numClickT = numClickT + 1;
      }
      var tranlateAtual = widthAxisX * numClickT;

      //ANIMACAO DESATIVADA
      /*bar.animate({transform: 't-'+tranlateAtual+',0'}, 500);
      AxisXLabels.animate({transform: 't-'+tranlateAtual+',0r-60'}, 500);
      this.buttonSet.animate({transform: 't-'+tranlateAtual+',0'}, 500);*/

      bar.transform('t-'+tranlateAtual+',0');
      AxisXLabels.transform('t-'+tranlateAtual+',0r-60');
    }

    this.navPrev = function(){
      numClickT = numClickT - 1;
      if(numClickT < 1){
        var tranlateAtual = 0;
        numClickT = 0;
      } else {
        var tranlateAtual = widthAxisX * numClickT;
      }

      //ANIMACAO DESATIVADA
      /*bar.animate({transform: 't-'+tranlateAtual+',0'}, 500);
      AxisXLabels.animate({transform: 't-'+tranlateAtual+',0r-60'}, 500);
      this.buttonSet.animate({transform: 't-'+tranlateAtual+',0'}, 500);*/

      bar.transform('t-'+tranlateAtual+',0');
      AxisXLabels.transform('t-'+tranlateAtual+',0r-60');
    }

    /*
    / INIT
    */
    chartBarLine.criarBarras();
    chartBarLine.criarLinhas();
  }

  Raphael.fn.iBarLine = function(w, h, valuesB, valuesL, opt) {
      return new ibarline(this, w, h , valuesB, valuesL, opt);
  }

})();