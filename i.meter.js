/*!
 * iChart - Charting library, based on Raphaël
 *
 * Copyright (c) 2012 Iuri Brito
 * Licensed under the MIT (http://www.opensource.org/licenses/mit-license.php) license.
 */

(function () {

    var img;

    function Indicador(paper, percent) {
        var px = 235 / 2 - 85;
        img = paper.image("/assets/universal/seta.png", px, 71, 85, 12);
        var angle = percent;
        console.log(angle);
        var angle2graus = (angle * 180) / 100
        var needleBox = img.getBBox();
        var x_rotate_point = needleBox.x + (needleBox.width/2) + 40;
        var y_rotate_point = needleBox.y + needleBox.height - 5;
        img.animate({transform: "R"+angle2graus+","+x_rotate_point+","+y_rotate_point}, 1000, "<>");
    }

    Raphael.fn.changeIndicador = function(percent) {
        img.transform("");
        var angle = percent;
        var angle2graus = (angle * 180) / 100
        var needleBox = img.getBBox();
        var x_rotate_point = needleBox.x + (needleBox.width/2) + 40;
        var y_rotate_point = needleBox.y + needleBox.height - 5;
        img.animate({transform: "R"+angle2graus+","+x_rotate_point+","+y_rotate_point}, 1000, "<>");
    }

    Raphael.fn.indicador = function(percent) {
        return new Indicador(this, percent);
    }

})();
