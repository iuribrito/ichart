/*!
 * iChart - Charting library, based on Raphaël
 *
 * Copyright (c) 2012 Iuri Brito
 * Licensed under the MIT (http://www.opensource.org/licenses/mit-license.php) license.
 */

(function () {
  function iwaterflow(p, width, height, ArrValues, opt) {
    var green = "90-#5B8433-#8CC53E", orange = "90-#E37201-#FC9B03", blue = "90-#006699-#00AEEF";
    var colors = ["90-#5B8433-#8CC53E", "90-#E37201-#FC9B03", "90-#006699-#00AEEF"];
    var widthBar = 0;
    var paddingTop = 20;
    var linhasBack = 10;

    var chartFlow = this;

    var values = [];
    values = clone(ArrValues);

    height = height - 20;

    var iBarChart = p.rect(30, 60, width, height);
    var iBarInfo = iBarChart.getBBox();
    var maxValue = 80;//Math.max.apply( Math, values[0] );
    var minValue = minValueArray(values);//Math.min.apply( Math, values[0] );
    var minPainel = 0;
    var maxPainel = maxValue + paddingTop;
    var valueUnitAxisY = maxPainel / linhasBack;
    var maxItensTable = 14;
    var widthAxisX = (maxItensTable <= values.length)? width / maxItensTable : width / values.length;
    var numClickT = 0;
    var clickMax = values.length - maxItensTable;
    var posBar = 0;

    var bar = p.set();
    var areaBars = p.set();
    var AxisXLabels = p.set();
    var valuesLabels = p.set();
    var buttonsColumn = p.set();
    var labelsBars = p.set();

    var groupButtons = [];
    var valorColuna = [];

    heightBackBar = height / linhasBack;

    labelsBars.push(p.text(iBarInfo.x + 30, iBarInfo.y - 50, "Receita")
      .attr({font: "16px 'Open Sans', Open-Sans, sans-serif", fill: colors[0], cursor: "pointer", "font-weight": "600"})
      .click(function(){
        if(this.selected){
          this.selected = false;
        } else {
          this.selected = true;
        }

        chartFlow.ocultarBar();
      })
    );
    labelsBars.push(p.text(iBarInfo.x + 100, iBarInfo.y - 50, "Despesa")
      .attr({font: "16px 'Open Sans', Open-Sans, sans-serif", fill: colors[1], cursor: "pointer", "font-weight": "600"})
      .click(function(){
        if(this.selected){
          this.selected = false;
        } else {
          this.selected = true;
        }

        chartFlow.ocultarBar();
      })
    );
    labelsBars.push(p.text(iBarInfo.x + 180, iBarInfo.y - 50, "Resultado")
      .attr({font: "16px 'Open Sans', Open-Sans, sans-serif", fill: colors[2], cursor: "pointer", "font-weight": "600"})
      .click(function(){
        if(this.selected){
          this.selected = false;
        } else {
          this.selected = true;
        }

        chartFlow.ocultarBar();
      })
    );

    /*
    / Desenhar Background
    */

    for(var i = 0; i <= linhasBack; i++){
      if(i % 2 == 0 && i != linhasBack){
        p.rect(iBarInfo.x + 1, iBarInfo.y + heightBackBar * i, iBarInfo.width - 2, heightBackBar).attr({fill: "#eaeaea", "stroke-width": 0});
      }

      var txtattr = { font: "11px 'Fontin Sans', Fontin-Sans, sans-serif" };
      L = p.text(iBarInfo.x - 15, (iBarInfo.y + iBarInfo.height) - heightBackBar * i , Math.round(valueUnitAxisY * i)).attr(txtattr);
    }

    p.path("M"+(iBarInfo.x + 0.5)+","+(iBarInfo.y - 30 + 0.5)+"L"+(iBarInfo.x+iBarInfo.width + 0.5)+","+(iBarInfo.y - 30 + 0.5)).attr({stroke: "#aaaaaa"});
    p.path("M"+iBarInfo.x+","+iBarInfo.y+"L"+(iBarInfo.x+iBarInfo.width)+","+iBarInfo.y).attr({stroke: "#aaaaaa"});
    p.path("M"+iBarInfo.x+","+(iBarInfo.y+iBarInfo.height)+"L"+(iBarInfo.x+iBarInfo.width)+","+(iBarInfo.y+iBarInfo.height)).attr({stroke: "#aaaaaa"});

    /*
    / Açoes nas Colunas
    */
    for(var i = 0; i < values.length; i++){
      areaBars.push(barTemp = p.rect(iBarInfo.x + widthAxisX * i, iBarInfo.y, widthAxisX, iBarInfo.height)).attr({fill: '#444', opacity: '0'});
      areaBars[i].values = [];
      for(var j = 0; j < values.length; j++){
        areaBars[i].values.push(values[j][i]);
      }
    }

    /*
    / Criar Barras e Labels
    */

    this.criarBarras = function(){
      bar.remove();
      valuesLabels.remove();
      AxisXLabels.remove();
      bar = p.set();
      iBarChart.attr({"stroke-width": 0});
      widthBar = (widthAxisX) / values.length;
      widthAxisX = (maxItensTable <= values.length)? width / maxItensTable : width / values.length;
      
      for(var i = 0; i < values.length; i++ ) {
        var colorBar;
        splitValue = values[i].split('-');
        heightPercent = 100 * parseInt(splitValue[1]) / maxPainel;
        heightPercentStart = 100 * parseInt(splitValue[0]) / maxPainel;
        heightPixelStart = height * heightPercentStart / 100;
        heightPixel = height * heightPercent / 100;
        posY = (iBarInfo.y + height - heightPixel) - heightPixelStart;

        if(splitValue[2] == "r"){
          colorBar = colors[2];
          label = "RESULTADO";
        } else if(splitValue[2] == "d") {
          colorBar = colors[1];
          label = "DESPESA";
        } else {
          colorBar = colors[0];
          label = "RECEITA";
        }

        bar.push(p.roundedRectangle(iBarInfo.x + widthAxisX * i, posY, widthAxisX, heightPixel, 0, 0, 0, 0)
                .attr({"stroke-width": '0', fill: colorBar})
        );

        bb = bar[i].getBBox();

        // Labels
        L = p.text(bb.x + bb.width / 2 , bb.y - 10, splitValue[1]).attr({ font: "12px 'Fontin Sans', Fontin-Sans, sans-serif", fill: "#666666" });
        valuesLabels.push(L);
        txtattr = { font: "10px 'Open Sans', Fontin-Sans, sans-serif", transform: "r-60", fill: colorBar };
        L = p.text(bb.x + bb.width / 2 , iBarInfo.height + 90, label).attr(txtattr);
        AxisXLabels.push(L);

        if(splitValue[2] == "r") {
          p.path("M"+iBarInfo.x+","+bb.y+"L"+(iBarInfo.x+iBarInfo.width)+","+bb.y).attr({stroke: "#006699"});
        }
      }
    }

    /*
    / Botoes de Comentarios
    */

    for(var i = 0; i < areaBars.length; i++){
      this.buttonsColumn = p.set();
      var ElementBarArea = areaBars[i].getBBox();

      this.buttonSet =  p.rect(ElementBarArea.x, ElementBarArea.y - 30, widthAxisX, 30).attr({fill: "#ccc", 'fill-opacity': 0  , stroke: '#ccc','stroke-width': 1, 'stroke-opacity':0});;
      this.buttonSet.back = p.rect(ElementBarArea.x + 0.5, ElementBarArea.y - 30 + 0.5, widthAxisX, 30).attr({'stroke-width': 1, stroke: 'transparent', fill: '#fff'});
      var elementData = this.buttonSet.getBBox();
      this.buttonSet.icon = p.image("/assets/universal/comment.png", elementData.x + elementData.width / 2 - 8, elementData.y + elementData.height / 2 - 7, 16, 15).attr({opacity: .6});
      this.buttonSet.toFront();

      this.buttonsColumn.push(this.buttonSet);

      this.buttonSet.mouseover(function(){
        this.attr({cursor: 'pointer'});
        this.back.attr({fill: "90-#EAEAEA-#FFFFFF", stroke: '#ccc'});
        this.icon.attr({opacity: 1});
      })
      .mouseout(function(){
        this.back.attr({fill: "#fff", stroke: 'transparent'});
        this.icon.attr({opacity: .6});
      })
      .click(function(){
        alert('faz alguma coisa');
      });
    }

    this.ocultarBar = function(){
      values = [];
      console.log(values);

      this.flow1 = labelsBars[0].selected;
      this.flow2 = labelsBars[1].selected;
      this.flow3 = labelsBars[2].selected;

      for(var i =0; i < ArrValues.length; i++){
        var splitValues = ArrValues[i].split('-');

        if(splitValues[2] == 'u' && !this.flow1) {
          values.push(ArrValues[i]);
        }
        if(splitValues[2] == 'd' && !this.flow2){
          values.push(ArrValues[i]);
        }
        if(splitValues[2] == 'r' && !this.flow3){
          values.push(ArrValues[i]);
        }
      }

      console.log(values);
      chartFlow.criarBarras();
    }

    /*
    / Navegaçao da Barra do Grafico
    */
      
    this.navNext = function(){
      if(clickMax != numClickT){
        numClickT = numClickT + 1;
      }
      var tranlateAtual = widthAxisX * numClickT;

      //ANIMACAO DESATIVADA
      /*bar.animate({transform: 't-'+tranlateAtual+',0'}, 500);
      AxisXLabels.animate({transform: 't-'+tranlateAtual+',0r-60'}, 500);
      this.buttonSet.animate({transform: 't-'+tranlateAtual+',0'}, 500);*/

      bar.transform('t-'+tranlateAtual+',0');
      AxisXLabels.transform('t-'+tranlateAtual+',0r-60');
      valuesLabels.transform('t-'+tranlateAtual+',0');
    }

    this.navPrev = function(){
      numClickT = numClickT - 1;
      if(numClickT < 1){
        var tranlateAtual = 0;
        numClickT = 0;
      } else {
        var tranlateAtual = widthAxisX * numClickT;
      }

      //ANIMACAO DESATIVADA
      /*bar.animate({transform: 't-'+tranlateAtual+',0'}, 500);
      AxisXLabels.animate({transform: 't-'+tranlateAtual+',0r-60'}, 500);
      this.buttonSet.animate({transform: 't-'+tranlateAtual+',0'}, 500);*/

      bar.transform('t-'+tranlateAtual+',0');
      AxisXLabels.transform('t-'+tranlateAtual+',0r-60');
      valuesLabels.transform('t-'+tranlateAtual+',0');
    }

    /*
    /INIT
    */  

    this.criarBarras();
    areaBars.toFront();

  }

  Raphael.fn.iWaterflow = function(w, h, values, opt) {
      return new iwaterflow(this, w, h , values, opt);
  }

})();