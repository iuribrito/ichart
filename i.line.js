/*!
 * iChart - Charting library, based on Raphaël
 *
 * Copyright (c) 2012 Iuri Brito
 * Licensed under the MIT (http://www.opensource.org/licenses/mit-license.php) license.
 */

(function () {
  function iline(p, width, height, values, opt) {

    var green = "90-#5B8433-#8CC53E", orange = "90-#E37201-#FC9B03", blue = "90-#006699-#00AEEF";
    var colors = ["#8CC53E", "#FC9B03", "#00AEEF"];
    
    var widthBar = 0;
    var paddingTop = 30;
    var linhasBack = 4;

    var iBarChart = p.rect(30, 20, width, height);
    var iBarInfo = iBarChart.getBBox();
    var maxValue = maxValueArray(values);//Math.max.apply( Math, values[0] );
    var minValue = minValueArray(values);//Math.min.apply( Math, values[0] );
    var minPainel = 0;
    var maxPainel = maxValue + paddingTop;
    var valueUnitAxisY = maxPainel / linhasBack;
    var widthAxisX = width / values[0].length;
    var bar = p.set();
    var areaBars = p.set();
    var posBar = 0;
    var valorColuna = [];
    var strPath = [];
    var circleLine = p.set();

    heightBackBar = height / linhasBack;

    for(var i = 0; i <= linhasBack; i++){
      if(i % 2 == 0 && i != linhasBack){
        p.rect(iBarInfo.x + 1, iBarInfo.y + heightBackBar * i, iBarInfo.width - 2, heightBackBar).attr({fill: "#eaeaea", "stroke-width": 0});
      }

      var txtattr = { font: "11px 'Fontin Sans', Fontin-Sans, sans-serif" };
      L = p.text(iBarInfo.x - 15, (iBarInfo.y + iBarInfo.height) - heightBackBar * i , Math.round(valueUnitAxisY * i)).attr(txtattr);
    }

    p.path("M"+iBarInfo.x+","+iBarInfo.y+"L"+(iBarInfo.x+iBarInfo.width)+","+iBarInfo.y).attr({stroke: "#aaaaaa"});
    p.path("M"+iBarInfo.x+","+(iBarInfo.y+iBarInfo.height)+"L"+(iBarInfo.x+iBarInfo.width)+","+(iBarInfo.y+iBarInfo.height)).attr({stroke: "#aaaaaa"});

    for(var i = 1; i < values[0].length; i++) {
      p.path("M"+(iBarInfo.x+widthAxisX * i)+","+iBarInfo.y+"L"+(iBarInfo.x+widthAxisX * i)+","+(iBarInfo.y+iBarInfo.height)).attr({stroke: "#aaaaaa"});
    }

    for(var i = 0; i < values[0].length; i++){
      areaBars.push(barTemp = p.rect(iBarInfo.x + widthAxisX * i, iBarInfo.y, widthAxisX, iBarInfo.height)).attr({fill: '#444', opacity: '0'});
      areaBars[i].values = [];
      for(var j = 0; j < values.length; j++){
        areaBars[i].values.push(values[j][i]);
      }
    }

    for(var i = 0; i < areaBars.length; i++){        
      areaBars[i].mouseover(function(){
        this.label = p.set();
        var element = this;

        for(var i = 0; i < element.values.length; i++){
          var boxColunas = element.getBBox();
          var multi = (15 * i) + boxColunas.y + 20;
          var color = ["#006699", "#66CC00", "#FF6600", "transparent"];
          
          txtattr = { font: "14px 'Fontin Sans', Fontin-Sans, sans-serif", fill: color[i] };
          this.label.push(p.text(boxColunas.x + (boxColunas.width / 2), multi, element.values[i]).attr(txtattr));
        }
      }).mouseout(function(){
        this.label.remove();
      });
    }

    iBarChart.attr({"stroke-width": 0});
    /*widthBar = (widthAxisX - 40) / values.length;
    for(var i = 0; i < values.length; i++ ) {
      for(var j = 0; j < values[i].length; j++ ){
        var labelValue = p.set();
        heightPercent = 100 * values[i][j] / maxPainel;
        heightPixel = height * heightPercent / 100;
        posBar = ((widthAxisX * (j + 1) - widthAxisX) + 20) + widthBar * i;
        bar.push(p.path("M"+(iBarInfo.x)+",60L"+(iBarInfo.x + posBar)+",140"));
        //bar.push(p.roundedRectangle(iBarInfo.x + posBar, iBarInfo.y + height - heightPixel, widthBar, heightPixel, 5, 5, 0, 0)
        //        .attr({"stroke-width": '0', fill: colors[i]})
        //);
      }
    } */

    widthBar = (widthAxisX - 40) / values.length;
    for(var i = 0; i < values.length; i++ ) {
      strPath = [];
      for(var j = 0; j < values[i].length; j++ ){
        heightPercent = 100 * values[i][j] / maxPainel;
        heightPixel = iBarInfo.y + height - (height * heightPercent / 100);
        posBar = iBarInfo.x + (widthAxisX * j) + (widthAxisX / 2);


        strPath.push(posBar+","+ heightPixel);
        circleLine.push(p.circle(posBar, heightPixel, 5).attr({fill: colors[i], stroke: "#fff"}));
      }
      var newStringPath = "";
      for(var l = 0; l < strPath.length; l++){
        if(l == 0) {
          newStringPath += "M"+strPath[l]+" R";
        } else if( l == strPath.length - 1) {
          newStringPath += strPath[l];
        } else {
          newStringPath += strPath[l]+" ";
        }
      }
      p.path(newStringPath).attr({stroke: colors[i], "stroke-width": 2});
    }
    circleLine.toFront();
    areaBars.toFront();
    labels = ['JAN', 'FEV', 'MAR', 'ABR', 'MAI', 'JUN', 'JUL', 'AGO', 'SET', 'OUT'];
    for ( var i = 0; i < labels.length; i++) {
      var barL = areaBars[i], 
          bb = barL.getBBox(),
          txtattr = { font: "11px 'Fontin Sans', Fontin-Sans, sans-serif"};

      if(labels[i]) {
        L = p.text((bb.x) + widthAxisX / 2, bb.y + bb.height + 20, labels[i]).attr(txtattr);
      }
    }
  }

  Raphael.fn.iLine = function(w, h, values, opt) {
      return new iline(this, w, h , values, opt);
  }

})();