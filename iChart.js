Raphael.fn.roundedRectangle = function (x, y, w, h, r1, r2, r3, r4){
  var array = [];
  array = array.concat(["M",x,r1+y, "Q",x,y, x+r1,y]); //A
  array = array.concat(["L",x+w-r2,y, "Q",x+w,y, x+w,y+r2]); //B
  array = array.concat(["L",x+w,y+h-r3, "Q",x+w,y+h, x+w-r3,y+h]); //C
  array = array.concat(["L",x+r4,y+h, "Q",x,y+h, x,y+h-r4, "Z"]); //D
  return this.path(array);
};

Array.prototype.remove = function(element) {
  this.splice(element, 1);
  return this;
}

Array.prototype.clone = function(arrOriginal) {
	var arrTemp = [];

  for(var i = 0; i < arrOriginal.length; i++){
  	arrTemp.push(arrOriginal[i]);
  }
  console.log(arrTemp);
  return arrTemp;
}

function clone(arrOriginal){
	var arrTemp = [];

  for(var i = 0; i < arrOriginal.length; i++){
  	arrTemp.push(arrOriginal[i]);
  }
  console.log(arrTemp);
  return arrTemp;
}

function maxValueArray(arrayMulti) {
  var max = 0;
  for(var i = 0; i < arrayMulti.length; i++){
    for(var j = 0; j < arrayMulti[i].length; j++){
      if(arrayMulti[i][j] > max) {
        max = arrayMulti[i][j];
      }
    }
  }
  return max;
}

function minValueArray(arrayMulti) {
  var min = arrayMulti[0][0];
  for(var i = 0; i < arrayMulti.length; i++){
    for(var j = 0; j < arrayMulti[i].length; j++){
      if(arrayMulti[i][j] < min) {
        min = arrayMulti[i][j];
      }
    }
  }
  return min;
}