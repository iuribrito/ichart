/*!
 * iChart - Charting library, based on Raphaël
 *
 * Copyright (c) 2012 Iuri Brito
 * Licensed under the MIT (http://www.opensource.org/licenses/mit-license.php) license.
 */

(function () {
  function ibar(p, width, height, ArrValues, opt) {
    var green = "90-#5B8433-#8CC53E", orange = "90-#E37201-#FC9B03", blue = "90-#006699-#00AEEF";
    var colorsConstant = ["90-#5B8433-#8CC53E", "90-#E37201-#FC9B03", "90-#006699-#00AEEF"];
    var colors = clone(colorsConstant);
    var widthBar = 0;
    var paddingTop = 20;
    var linhasBack = 4;

    var chartBar = this;
    
    var values = [];
    values = clone(ArrValues);

    var iBarChart = p.rect(30, 60, width, height);
    var iBarInfo = iBarChart.getBBox();
    var maxValue = maxValueArray(values);//Math.max.apply( Math, values[0] );
    var minValue = minValueArray(values);//Math.min.apply( Math, values[0] );
    var minPainel = 0;
    var maxPainel = maxValue + paddingTop;
    var valueUnitAxisY = maxPainel / linhasBack;
    var maxItensTable = 6;
    var widthAxisX = (maxItensTable <= values[0].length)? width / maxItensTable : width / values[0].length;
    var numClickT = 0;
    var clickMax = values[0].length - maxItensTable;
    var posBar = 0;

    var bar = p.set();
    var areaBars = p.set();
    var AxisXLabels = p.set();
    var buttonsColumn = p.set();
    var labelsBars = p.set();
    
    var groupButtons = [];
    var valorColuna = [];
    

    heightBackBar = height / linhasBack;

    labelsBars.push(p.text(iBarInfo.x + 30, iBarInfo.y - 50, "Item 1")
      .attr({font: "16px 'Open Sans', Open-Sans, sans-serif", fill: colors[0], cursor: "pointer", "font-weight": "600"})
      .click(function(){
        if(this.selected){
          this.selected = false;
        } else {
          this.selected = true;
        }

        chartBar.ocultarBar();
      })
    );
    labelsBars.push(p.text(iBarInfo.x + 90, iBarInfo.y - 50, "Item 2")
      .attr({font: "16px 'Open Sans', Open-Sans, sans-serif", fill: colors[1], cursor: "pointer", "font-weight": "600"})
      .click(function(){
        if(this.selected){
          this.selected = false;
        } else {
          this.selected = true;
        }

        chartBar.ocultarBar();
      })
    );
    labelsBars.push(p.text(iBarInfo.x + 150, iBarInfo.y - 50, "Item 3")
      .attr({font: "16px 'Open Sans', Open-Sans, sans-serif", fill: colors[2], cursor: "pointer", "font-weight": "600"})
      .click(function(){
        if(this.selected){
          this.selected = false;
        } else {
          this.selected = true;
        }

        chartBar.ocultarBar();
      })
    );

    /*
    / Desenhar Background
    */

    //Linhas(barras)
    for(var i = 0; i <= linhasBack; i++){
      if(i % 2 == 0 && i != linhasBack){
        p.rect(iBarInfo.x + 1, iBarInfo.y + heightBackBar * i, iBarInfo.width - 2, heightBackBar).attr({fill: "#eaeaea", "stroke-width": 0});
      }

      var txtattr = { font: "11px 'Fontin Sans', Fontin-Sans, sans-serif" };
      L = p.text(iBarInfo.x - 15, (iBarInfo.y + iBarInfo.height) - heightBackBar * i , Math.round(valueUnitAxisY * i)).attr(txtattr);
    }

    p.path("M"+(iBarInfo.x + 0.5)+","+(iBarInfo.y - 30 + 0.5)+"L"+(iBarInfo.x+iBarInfo.width + 0.5)+","+(iBarInfo.y - 30 + 0.5)).attr({stroke: "#aaaaaa"});

    //Topo e Baixo
    p.path("M"+(iBarInfo.x + 0.5)+","+(iBarInfo.y + 0.5)+"L"+(iBarInfo.x+iBarInfo.width + 0.5)+","+(iBarInfo.y + 0.5)).attr({stroke: "#aaaaaa"});
    p.path("M"+(iBarInfo.x + 0.5)+","+(iBarInfo.y+iBarInfo.height + 0.5)+"L"+(iBarInfo.x+iBarInfo.width + 0.5)+","+(iBarInfo.y+iBarInfo.height + 0.5)).attr({stroke: "#aaaaaa"});

    //Colunas
    for(var i = 1; i < values[0].length; i++) {
      p.path("M"+(iBarInfo.x+widthAxisX * i + 0.5)+","+(iBarInfo.y + 0.5)+"L"+(iBarInfo.x+widthAxisX * i + 0.5)+","+(iBarInfo.y+iBarInfo.height + 0.5)).attr({stroke: "#aaaaaa"});
    }

    /*
    / Açoes nas Colunas e Label com os valores
    */

    for(var i = 0; i < values[0].length; i++){
      areaBars.push(barTemp = p.rect(iBarInfo.x + widthAxisX * i, iBarInfo.y, widthAxisX, iBarInfo.height)).attr({fill: '#444', opacity: '0'});
      areaBars[i].values = [];
      for(var j = 0; j < values.length; j++){
        areaBars[i].values.push(values[j][i]);
      }
    }

    for(var i = 0; i < areaBars.length; i++){
      areaBars[i].mouseover(function(){
        this.label = p.set();
        var element = this;

        for(var i = 0; i < element.values.length; i++){
          var boxColunas = element.getBBox();
          var multi = (15 * i) + boxColunas.y + 20;
          var color = ["#006699", "#66CC00", "#FF6600", "transparent"];

          element.attr({cursor: 'pointer'});
          
          txtattr = { font: "14px 'Fontin Sans', Fontin-Sans, sans-serif", fill: color[i] };
          this.label.push(p.text(boxColunas.x + (boxColunas.width / 2), multi, element.values[i]).attr(txtattr));
        }
      }).mouseout(function(){
        this.label.remove();
      }).click(function(){
        window.location = "http://www.bemind.com.br";
      });
    }

    /*
    / Botoes de Comentarios
    */

    for(var i = 0; i < areaBars.length; i++){
      this.buttonsColumn = p.set();
      var ElementBarArea = areaBars[i].getBBox();

      this.buttonSet =  p.rect(ElementBarArea.x, ElementBarArea.y - 30, widthAxisX, 30).attr({fill: "#ccc", 'fill-opacity': 0  , stroke: '#ccc','stroke-width': 1, 'stroke-opacity':0});;
      this.buttonSet.back = p.rect(ElementBarArea.x + 0.5, ElementBarArea.y - 30 + 0.5, widthAxisX, 30).attr({'stroke-width': 1, stroke: 'transparent', fill: '#fff'});
      var elementData = this.buttonSet.getBBox();
      this.buttonSet.icon = p.image("/assets/universal/comment.png", elementData.x + elementData.width / 2 - 8, elementData.y + elementData.height / 2 - 7, 16, 15).attr({opacity: .6});
      this.buttonSet.toFront();

      this.buttonsColumn.push(this.buttonSet);

      this.buttonSet.mouseover(function(){
        this.attr({cursor: 'pointer'});
        this.back.attr({fill: "90-#EAEAEA-#FFFFFF", stroke: '#ccc'});
        this.icon.attr({opacity: 1});
      })
      .mouseout(function(){
        this.back.attr({fill: "#fff", stroke: 'transparent'});
        this.icon.attr({opacity: .6});
      })
      .click(function(){
        alert('faz alguma coisa');
      });
    }

    /*
    / Barras laterais, delimitar area do grafico
    */



    /*
    / Ocultar todas as Barras
    */

    this.ocultarBar = function(){
      values = [];
      colors = [];
      console.log(values);
      for(var i =0; i < labelsBars.length; i++){
        console.log(labelsBars[i].selected);
        if(!labelsBars[i].selected){
          values.push(ArrValues[i]);
          colors.push(colorsConstant[i]);
        }
      }

      console.log(values);
      chartBar.criarBarras();
    }

    /*
    / Criar Barras do Grafico
    */

    this.criarBarras = function(){
      bar.remove();
      bar = p.set();
      iBarChart.attr({"stroke-width": 0});
      widthBar = (widthAxisX - 40) / values.length;

      for(var i = 0; i < values.length; i++ ) {
        for(var j = 0; j < values[i].length; j++ ){
          var labelValue = p.set();
          heightPercent = 100 * values[i][j] / maxPainel;
          heightPixel = height * heightPercent / 100;
          posBar = ((widthAxisX * (j + 1) - widthAxisX) + 20) + widthBar * i;
          bar.push(p.roundedRectangle(iBarInfo.x + posBar, iBarInfo.y + height - heightPixel, widthBar, heightPixel, 5, 5, 0, 0)
                    .attr({"stroke-width": '0', fill: colors[i]})
          );
        }
      }
    }

    /*
    / Retorna True ou False, caso o grafico precise ou nao de rolagem
    */

    this.hasRoll = function(){
      if(maxItensTable > values[0].length) {
        return true;
      } else {
        return false;
      }
    }

    /*
    / Definiçoes dos Labels do Eixo X
    */

    this.criarLabels = function(){
      areaBars.toFront();
      labels = ['JAN', 'FEV', 'MAR', 'ABR', 'MAI', 'JUN', 'JUL'];
      for ( var i = 0; i < labels.length; i++) {
        var barL = bar[i], 
            bb = barL.getBBox(),
            txtattr = { font: "11px 'Fontin Sans', Fontin-Sans, sans-serif", transform: "r-60" };

        if(labels[i]) {
          L = p.text((bb.x - 20) + widthAxisX / 2, bb.y + bb.height + 20, labels[i]).attr(txtattr);
          AxisXLabels.push(L);
        }
      }
    }

    /*
    / Navegaçao da Barra do Grafico
    */
      
    this.navNext = function(){
      if(clickMax != numClickT){
        numClickT = numClickT + 1;
      }
      var tranlateAtual = widthAxisX * numClickT;

      //ANIMACAO DESATIVADA
      /*bar.animate({transform: 't-'+tranlateAtual+',0'}, 500);
      AxisXLabels.animate({transform: 't-'+tranlateAtual+',0r-60'}, 500);
      this.buttonSet.animate({transform: 't-'+tranlateAtual+',0'}, 500);*/

      bar.transform('t-'+tranlateAtual+',0');
      AxisXLabels.transform('t-'+tranlateAtual+',0r-60');
    }

    this.navPrev = function(){
      numClickT = numClickT - 1;
      if(numClickT < 1){
        var tranlateAtual = 0;
        numClickT = 0;
      } else {
        var tranlateAtual = widthAxisX * numClickT;
      }

      //ANIMACAO DESATIVADA
      /*bar.animate({transform: 't-'+tranlateAtual+',0'}, 500);
      AxisXLabels.animate({transform: 't-'+tranlateAtual+',0r-60'}, 500);
      this.buttonSet.animate({transform: 't-'+tranlateAtual+',0'}, 500);*/

      bar.transform('t-'+tranlateAtual+',0');
      AxisXLabels.transform('t-'+tranlateAtual+',0r-60');
    }

    /*
    /INIT
    */

    this.criarBarras();
    this.criarLabels();
  }

  Raphael.fn.iBar = function(w, h, values, opt) {
    return new ibar(this, w, h , values, opt);
  }
})();